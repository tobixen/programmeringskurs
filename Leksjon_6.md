# Leksjon 6 - programmering vha editor

I denne leksjonen skal vi organisere noen av eksemplene fra tidligere leksjoner i filer.

## Egen katalog for leksjoner

Det er en god idé å samle filer som hører sammen i egne kataloger.

Her kan man lage en avansert katalogstruktur om man ønsker, f.eks. kan leksjonene legges inn i ~/programmering/python/programmeringskurs/ - men jeg anbefaler heller å lage en katalog med kortere navn og mindre dybde, f.eks. ~/kurs

### Oppgaver

* Lag en katalog for programmeringskurset og gå til den, bruk kommandoene fra [Leksjon 5](Leksjon_5.mediawiki)

* Bruk en editor, lag en fil kurs.py og legg inn definisjonene rektangel og mangekant fra [Leksjon 4](Leksjon_4.mediawiki).  Lagre.

## Import av bibliotek

Start opp python, og du kan skrive:

    import kurs
    kurs.rektangel(30,70)

Du kan også skrive:

    from kurs import rektangel,mangekant
    rektangel(30,70)
    mangekant(14,140)

Enda enklere (men generelt ikke anbefalt):

    from kurs import *
    rektangel(20,20)

### Oppgave

Lag en programfil, test.py, på første linje skal du ha #!/usr/bin/python, lengre ned skal du importere kurs-biblioteket, deretter bruke noen av kommandoene du har importert.  Lag fila eksekverbar, se [Leksjon 4](Leksjon_4.mediawiki), og kjør den.

## Kombinere bibliotek og eksekverbar kode

Det er smart å legge mest mulig av koden i funksjoner.  Det finnes en spesialvariabel __name__ som er satt til '__main__' dersom man eksekverer koden.  Dermed kan man gjøre ting som dette:

    #!/usr/bin/python
    from kurs import rektangel

    def main():
        rektangel(30,50)

    if __name__ == '__main__':
        main()

### Oppgaver

* Legg koden over i test.py.  Prøv å kjøre "import test" i python.  Hva skjer?  Prøv å kjøre fila - hva skjer?

* Fiks programmet slik at det tegner et hus når man kjører det.  Programmet skal ikke gjøre noe dersom man importerer det.

* Kjør python interaktivt, importer test og tegn et hus

## Revisjonskontroll - git

Av og til gjør man feil slik at programmet slutter å virke.  Da er det nyttig å kunne gå tilbake og se hva man har gjort galt.  Til dette bruker man verktøyet git.

For å sette opp git i katalogen, skriver du:

    git init

For å legge til nye filer, bruker man "git add":

    git add kurs.py
    git add test.py

git commit for å "sjekke inn" endringer du har gjort.  -a for å legge til alle endringer.  -m for å legge med en beskrivelse:

    git commit -am "kurs og test, første versjon"

git kan være noe vanskelig.  Spør om hjelp hvis du har problemer med git.

For å se hvilke endringer man har gjort siden sist, bruker man disse to kommandoene:

    git status
    git diff

### Oppgave

Fiks test.py slik at den tegner to hus i stedet for ett.  Bruk git til å se på endringene som er gjort.  Bruk git til å sjekke inn endringen.

## Neste leksjon

I Leksjon 7 skal vi tegne bokstaven A, samt se bittelitt på såkalt "objektorientert programmering".
